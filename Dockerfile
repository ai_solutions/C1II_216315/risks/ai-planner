FROM maven:3.8.3-openjdk-17
ADD target target
ADD pom.xml .
EXPOSE 8855
ENTRYPOINT ["java","-jar","target/ai-solutions-solver-0.0.1-SNAPSHOT.jar"]
