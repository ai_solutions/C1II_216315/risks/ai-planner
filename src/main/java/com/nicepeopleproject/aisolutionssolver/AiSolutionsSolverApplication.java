package com.nicepeopleproject.aisolutionssolver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AiSolutionsSolverApplication {

	public static void main(String[] args) {
		SpringApplication.run(AiSolutionsSolverApplication.class, args);
	}

}
