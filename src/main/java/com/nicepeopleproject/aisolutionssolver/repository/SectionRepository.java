package com.nicepeopleproject.aisolutionssolver.repository;

import com.nicepeopleproject.aisolutionssolver.model.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SectionRepository extends JpaRepository<Section, Integer> {
}
