package com.nicepeopleproject.aisolutionssolver.repository;

import com.nicepeopleproject.aisolutionssolver.model.KnowledgeBase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KnowledgeBaseRepository extends JpaRepository<KnowledgeBase, Integer> {
}
