package com.nicepeopleproject.aisolutionssolver.repository;

import com.nicepeopleproject.aisolutionssolver.model.NodeActivationCondition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NodeActivationConditionRepository extends JpaRepository<NodeActivationCondition, Integer> {
}
