package com.nicepeopleproject.aisolutionssolver.repository;

import com.nicepeopleproject.aisolutionssolver.model.Node;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NodeRepository extends JpaRepository<Node, Integer> {
}
