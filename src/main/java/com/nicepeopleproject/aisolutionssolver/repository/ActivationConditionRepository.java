package com.nicepeopleproject.aisolutionssolver.repository;

import com.nicepeopleproject.aisolutionssolver.model.ActivationCondition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivationConditionRepository extends JpaRepository<ActivationCondition, Integer> {
}
