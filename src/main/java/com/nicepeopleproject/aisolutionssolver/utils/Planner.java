package com.nicepeopleproject.aisolutionssolver.utils;

import java.util.*;
import java.util.stream.Collectors;

public class Planner {
    public static List<String> planWorkOut(List<Exercise> allExercises, Map<String, Integer> userDiseaseRank, int metLimit, Location location) {
        List<String> exercises = new ArrayList<>();
        List<Exercise> approvedExercises = allExercises.stream()
                .filter(exercise -> exercise.getLocation().equals(location))
                .filter(exercise -> acceptableForUser(exercise, userDiseaseRank))
                .collect(Collectors.toList());

        Map<Exercise, Integer> exerciseUserImpact = new HashMap<>();
        for (Exercise exercise : approvedExercises) {
            exerciseUserImpact.put(exercise, exercise.getTotalPositiveImpact(userDiseaseRank.keySet()));
        }
        while (metLimit > 0 && exerciseUserImpact.size() > 0) {
            metLimit -= addNextMostUsefulExercise(exerciseUserImpact, metLimit, exercises);
        }

        return exercises;
    }

    private static int addNextMostUsefulExercise(Map<Exercise, Integer> exerciseUserImpact, int metLeft, List<String> exercises) {
        List<Exercise> mostImpactExercises = new ArrayList<>();
        Optional<Integer> maxImpact = exerciseUserImpact.values().stream().max(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });
        int metSpend = 0;
        mostImpactExercises = exerciseUserImpact.entrySet().stream()
                .filter(entry -> entry.getValue() == maxImpact.get())
                .map(entry -> entry.getKey())
                .collect(Collectors.toList());
        for (Exercise exercise : mostImpactExercises) {
            if (metLeft >= exercise.getMet()) {
                exercises.add(exercise.getDescription());
                metSpend += exercise.getMet();
            }
            exerciseUserImpact.remove(exercise);
        }
        return metSpend;
    }

    private static boolean acceptableForUser(Exercise exercise, Map<String, Integer> userDiseaseRank) {
        for (Map.Entry<String, Integer> entry : userDiseaseRank.entrySet()) {
            if (exercise.getNegativeImpactRank(entry.getKey()) <= entry.getValue()) {
                return false;
            }
        }
        return true;
    }
}


