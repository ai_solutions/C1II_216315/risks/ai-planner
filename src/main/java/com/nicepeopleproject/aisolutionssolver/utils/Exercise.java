package com.nicepeopleproject.aisolutionssolver.utils;

import java.util.Map;
import java.util.Set;

public class Exercise {
    private Location location;
    private String description;
    private Map<String, Integer> positiveImpactOnDisease;
    private Map<String, Integer> negativeImpactOnDisease;
    private int met;

    public Exercise(Location location, String description, Map<String, Integer> positiveImpactOnDisease, Map<String, Integer> negativeImpactOnDisease, int met) {
        this.location = location;
        this.description = description;
        this.positiveImpactOnDisease = positiveImpactOnDisease;
        this.negativeImpactOnDisease = negativeImpactOnDisease;
        this.met = met;
    }

    public Location getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public Map<String, Integer> getPositiveImpactOnDisease() {
        return positiveImpactOnDisease;
    }

    public Map<String, Integer> getNegativeImpactOnDisease() {
        return negativeImpactOnDisease;
    }

    public int getNegativeImpactRank(String disease) {
        return negativeImpactOnDisease.containsKey(disease) ? negativeImpactOnDisease.get(disease) : 0;
    }

    public int getTotalPositiveImpact(Set<String> diseases) {
        int totalImpact = 0;
        for (String disease : diseases) {
            if (positiveImpactOnDisease.containsKey(disease)) totalImpact++;
        }
        return totalImpact;
    }

    public int getMet() {
        return met;
    }
}
