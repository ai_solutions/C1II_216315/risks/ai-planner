package com.nicepeopleproject.aisolutionssolver.dto;

import models.Node;

public class NodeDto {
    private Integer id;
    private String name;

    public NodeDto(Node node){
        id = node.getId();
        name = node.getName();
    }

    public NodeDto(com.nicepeopleproject.aisolutionssolver.model.Node node){
        id = node.getId();
        name = node.getName();

    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
