package com.nicepeopleproject.aisolutionssolver.dto;

import com.nicepeopleproject.aisolutionssolver.model.NodeTypeEnum;
import models.Node;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class HypothesisDto {

    private Integer id;

    private String name;
    private List<NodeDto> explanatorySet;

    public HypothesisDto(Node node){
        this.id = node.getId();
        this.name = node.getName();

        this.explanatorySet = node.getExplanatorySet().stream().map(n -> new NodeDto(n)).collect(Collectors.toList());
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<NodeDto> getExplanatorySet() {
        return explanatorySet;
    }
}
