package com.nicepeopleproject.aisolutionssolver.controller;

import com.nicepeopleproject.aisolutionssolver.model.KnowledgeBase;
import com.nicepeopleproject.aisolutionssolver.model.Node;
import com.nicepeopleproject.aisolutionssolver.service.NodeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/node")
public class NodeController {
    private NodeService nodeService;

    public NodeController(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<Node> getAllNode() {
        return  nodeService.getAll();
    }
}
