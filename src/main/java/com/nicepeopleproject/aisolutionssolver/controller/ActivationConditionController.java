package com.nicepeopleproject.aisolutionssolver.controller;

import com.nicepeopleproject.aisolutionssolver.model.ActivationCondition;
import com.nicepeopleproject.aisolutionssolver.service.ActivationConditionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/activation_condition")
public class ActivationConditionController {
    private ActivationConditionService activationConditionService;

    public ActivationConditionController(ActivationConditionService activationConditionService) {
        this.activationConditionService = activationConditionService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<ActivationCondition> getAllActivationCondition() {
        return  activationConditionService.getAll();
    }
}
