package com.nicepeopleproject.aisolutionssolver.controller;

import argAlg.AlgorithmAdditionalStep;
import argAlg.ArgumentationAlgorithm;
import argAlg.CheckNodeStatusQuery;
import argAlg.HasPlace;
import com.nicepeopleproject.aisolutionssolver.dto.HypothesisDto;
import com.nicepeopleproject.aisolutionssolver.model.ActivationCondition;
import com.nicepeopleproject.aisolutionssolver.model.Connection;
import com.nicepeopleproject.aisolutionssolver.model.NodeActivationCondition;
import com.nicepeopleproject.aisolutionssolver.model.NodeTypeEnum;
import com.nicepeopleproject.aisolutionssolver.service.*;
import models.ArgumentationCurrentAlgorithmState;
import models.ConnectionType;
import models.Node;
import models.NodeType;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/solver")
public class SolverController {
    private KnowledgeBaseService knowledgeBaseService;
    private SectionService sectionService;
    private NodeService nodeService;
    private ConnectionService connectionService;
    private NodeActivationConditionService nodeActivationConditionService;
    private ActivationConditionService activationConditionService;

    public SolverController(KnowledgeBaseService knowledgeBaseService, SectionService sectionService, NodeService nodeService, ConnectionService connectionService, NodeActivationConditionService nodeActivationConditionService, ActivationConditionService activationConditionService) {
        this.knowledgeBaseService = knowledgeBaseService;
        this.sectionService = sectionService;
        this.nodeService = nodeService;
        this.connectionService = connectionService;
        this.nodeActivationConditionService = nodeActivationConditionService;
        this.activationConditionService = activationConditionService;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<HypothesisDto> getSolution(@RequestBody Map<String, String> labelValue) {
        AlgorithmAdditionalStep algorithmAdditionalStep = AlgorithmAdditionalStep.NONE;
        CheckNodeStatusQuery checkNodeStatusQuery = new CheckNodeStatusQuery() {
            @Override
            public HasPlace check(Node node) {
                return HasPlace.NO;
            }
        };
        ArgumentationAlgorithm argumentationAlgorithm = new ArgumentationAlgorithm(algorithmAdditionalStep, checkNodeStatusQuery);
        ArgumentationCurrentAlgorithmState currentAlgorithmState = new ArgumentationCurrentAlgorithmState();


        // addAll
        List<com.nicepeopleproject.aisolutionssolver.model.Node> allNodes = nodeService.getAll();
        allNodes.forEach(node -> {
            if (node.getNodeType() == NodeTypeEnum.full_activation_service_node) {
                currentAlgorithmState.getAll().add(new Node(node.getId(), node.getName(),true, NodeType.SERVICE_NODE));
            } else {
                NodeType nodeType = null;
                switch (node.getNodeType()) {
                    case target_node -> nodeType = NodeType.TARGET_NODE;
                    case feature_node -> nodeType = NodeType.FEATURE_NODE;
                    case full_activation_service_node, partial_activation_service_node ->
                            nodeType = NodeType.SERVICE_NODE;
                }
                currentAlgorithmState.getAll().add(new Node(node.getId(), node.getName(),false, nodeType));
            }
        });
        // addO
        // conver labels to ids
        Map<String, String> tmpLabelValue = new HashMap<>();
        List<ActivationCondition> allActivationConditions = activationConditionService.getAll();
        for (Map.Entry<String, String> labelValueEntry: labelValue.entrySet()){
            tmpLabelValue.put(allActivationConditions.stream().filter(c->c.getLabel().equals(labelValueEntry.getKey())).findFirst().get().getId().toString(),
                    labelValueEntry.getValue());
        }
        labelValue = tmpLabelValue;
        for (Map.Entry<String, String> labelValueEntry : labelValue.entrySet()) {
            for (NodeActivationCondition condition : nodeActivationConditionService.getAll()) {
                // by label not by id
                if (labelValueEntry.getKey().equals(condition.getActivationConditionId().toString())) {
                    if (condition.getNumericRangeStartPoint() == null) {

                        if (condition.getStringValues().contains(labelValueEntry.getValue())) {
                            currentAlgorithmState.getO().add(currentAlgorithmState.getAll().stream().filter(node -> node.getId() == condition.getNodeId()).findFirst().get());
                        }
                    } else {
                        Double value = Double.parseDouble(labelValueEntry.getValue());
                        //TODO : include END START must be realised
                        if (((value > condition.getNumericRangeStartPoint()) &
                                (value < condition.getNumericRangeEndPoint()))
                        ||(condition.getIncludeRangeStartPoint()&&value.equals(condition.getNumericRangeStartPoint()))
                                ||(condition.getIncludeRangeEndPoint()&&(value.equals(condition.getNumericRangeEndPoint())))) {
                            currentAlgorithmState.getO().add(currentAlgorithmState.getAll().stream().filter(node -> node.getId() == condition.getNodeId()).findFirst().get());
                        }
                    }
                }
            }
        }


        // addConnecetions
        List<Connection> connections = connectionService.getAll();
        connections.forEach(
                connection -> {
                    ConnectionType connectionType = null;
                    switch (connection.getConnectionType()) {
                        case TRA -> connectionType = ConnectionType.TRA;
                        case RS -> connectionType = ConnectionType.RS;
                        case S -> connectionType = ConnectionType.S;
                        case SN, ART -> throw new RuntimeException("SN,ART unsupported");
                    }

                    currentAlgorithmState.getConnections().add(new models.Connection(
                            currentAlgorithmState.getAll().stream().filter(node -> node.getId().equals(connection.getFromNode())).findFirst().get(),
                            currentAlgorithmState.getAll().stream().filter(node -> node.getId().equals(connection.getToNode())).findFirst().get(),
                            connectionType
                    ));
                }
        );

        argumentationAlgorithm.run(currentAlgorithmState);
//        return currentAlgorithmState.getH().stream().map(node -> allNodes.stream().filter(node1 -> node.getId().equals(node1.getId())).findFirst().get()).collect(Collectors.toList());
        return currentAlgorithmState.getH().stream().map(node-> new HypothesisDto(node)).collect(Collectors.toList());
    }

}
