package com.nicepeopleproject.aisolutionssolver.controller;

import com.nicepeopleproject.aisolutionssolver.model.KnowledgeBase;
import com.nicepeopleproject.aisolutionssolver.service.KnowledgeBaseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/kb")
public class KnowledgeBaseController {

    private KnowledgeBaseService knowledgeBaseService;

    public KnowledgeBaseController(KnowledgeBaseService knowledgeBaseService) {
        this.knowledgeBaseService = knowledgeBaseService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<KnowledgeBase> getAllKb() {
        return  knowledgeBaseService.getAll();
    }
}
