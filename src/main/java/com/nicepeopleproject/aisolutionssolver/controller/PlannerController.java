package com.nicepeopleproject.aisolutionssolver.controller;

import com.nicepeopleproject.aisolutionssolver.utils.Exercise;
import com.nicepeopleproject.aisolutionssolver.utils.Location;
import com.nicepeopleproject.aisolutionssolver.utils.Planner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/plan-workout")
public class PlannerController {
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<String> getSolution(@RequestBody List<Exercise> allExercises,
                                           @RequestBody Map<String, Integer> userDiseaseRank,
                                           @RequestBody int metLimit,
                                           @RequestBody Location location) {
        return Planner.planWorkOut(allExercises, userDiseaseRank, metLimit, location);
    }
}
