package com.nicepeopleproject.aisolutionssolver.controller;

import com.nicepeopleproject.aisolutionssolver.model.NodeActivationCondition;
import com.nicepeopleproject.aisolutionssolver.service.NodeActivationConditionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("node_activation_condition")
public class NodeActivationConditionController {
    private NodeActivationConditionService nodeActivationConditionService;

    public NodeActivationConditionController(NodeActivationConditionService nodeActivationConditionService) {
        this.nodeActivationConditionService = nodeActivationConditionService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<NodeActivationCondition> getAllNodeActivationCondition() {
        return  nodeActivationConditionService.getAll();
    }
}
