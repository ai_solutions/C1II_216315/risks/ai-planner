package com.nicepeopleproject.aisolutionssolver.model;

public enum NodeTypeEnum {
    target_node, full_activation_service_node, partial_activation_service_node, feature_node
}
