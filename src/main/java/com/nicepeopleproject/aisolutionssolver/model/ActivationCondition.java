package com.nicepeopleproject.aisolutionssolver.model;

import com.fasterxml.jackson.annotation.JsonValue;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "activation_conditions")
public class ActivationCondition {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "activation_condition_name")
    private String name;
    @Column(name = "has_numeric_type")
    private Boolean hasNumericType;
    @Column(name = "possible_numeric_range_start_point")
    private Double rangeStart;
    @Column(name = "possible_numeric_range_end_point")
    private Double rangeEnd;
    @Column(name = "possible_string_values")
    private String possibleStringValues;
    @Column(name = "label")
    private String label;
    @Column(name = "kb_id")
    private Integer kbId;
    @Column(name = "description")
    private String description;

    public ActivationCondition() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getHasNumericType() {
        return hasNumericType;
    }

    public void setHasNumericType(Boolean hasNumericType) {
        this.hasNumericType = hasNumericType;
    }

    public Double getRangeStart() {
        return rangeStart;
    }

    public void setRangeStart(Double rangeStart) {
        this.rangeStart = rangeStart;
    }

    public Double getRangeEnd() {
        return rangeEnd;
    }

    public void setRangeEnd(Double rangeEnd) {
        this.rangeEnd = rangeEnd;
    }

    public String getPossibleStringValues() {
        return possibleStringValues;
    }

    public void setPossibleStringValues(String possibleStringValues) {
        this.possibleStringValues = possibleStringValues;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getKbId() {
        return kbId;
    }

    public void setKbId(Integer kbId) {
        this.kbId = kbId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
