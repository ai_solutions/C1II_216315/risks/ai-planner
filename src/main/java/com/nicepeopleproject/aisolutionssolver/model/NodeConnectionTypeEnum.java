package com.nicepeopleproject.aisolutionssolver.model;

public enum NodeConnectionTypeEnum {
    RS, TRA, S, SN, ART
}
