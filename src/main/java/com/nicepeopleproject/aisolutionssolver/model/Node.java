package com.nicepeopleproject.aisolutionssolver.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "nodes")
public class Node {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "section_id")
    private Integer sectionId;
    @Column(name = "node_name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "node_type")
    private NodeTypeEnum nodeType;

    @Column(name = "description")
    private String description;


    public Node() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NodeTypeEnum getNodeType() {
        return nodeType;
    }

    public void setNodeType(NodeTypeEnum nodeType) {
        this.nodeType = nodeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Node{" +
                "id=" + id +
                ", sectionId=" + sectionId +
                ", name='" + name + '\'' +
                ", nodeType=" + nodeType +
                ", description='" + description + '\'' +
                '}';
    }
}
