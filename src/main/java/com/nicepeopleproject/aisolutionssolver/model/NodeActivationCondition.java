package com.nicepeopleproject.aisolutionssolver.model;

import jakarta.persistence.*;

@Entity
@Table(name = "node_activation_conditions")
public class NodeActivationCondition {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "node_id")
    private Integer nodeId;
    @Column(name = "activation_condition_id")
    private Integer activationConditionId;
    @Column(name = "numeric_range_start_point")
    private Double numericRangeStartPoint;
    @Column(name = "numeric_range_end_point")
    private Double numericRangeEndPoint;
    @Column(name = "include_start_point")
    private Boolean includeRangeStartPoint;
    @Column(name = "include_end_point")
    private Boolean includeRangeEndPoint;
    @Column(name = "string_values")
    private String stringValues;
    @Column(name = "description")
    private String description;

    public NodeActivationCondition() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public Integer getActivationConditionId() {
        return activationConditionId;
    }

    public void setActivationConditionId(Integer activationConditionId) {
        this.activationConditionId = activationConditionId;
    }

    public Double getNumericRangeStartPoint() {
        return numericRangeStartPoint;
    }

    public void setNumericRangeStartPoint(Double numericRangeStartPoint) {
        this.numericRangeStartPoint = numericRangeStartPoint;
    }

    public Double getNumericRangeEndPoint() {
        return numericRangeEndPoint;
    }

    public void setNumericRangeEndPoint(Double numericRangeEndPoint) {
        this.numericRangeEndPoint = numericRangeEndPoint;
    }

    public Boolean getIncludeRangeStartPoint() {
        return includeRangeStartPoint;
    }

    public void setIncludeRangeStartPoint(Boolean includeRangeStartPoint) {
        this.includeRangeStartPoint = includeRangeStartPoint;
    }

    public Boolean getIncludeRangeEndPoint() {
        return includeRangeEndPoint;
    }

    public void setIncludeRangeEndPoint(Boolean includeRangeEndPoint) {
        this.includeRangeEndPoint = includeRangeEndPoint;
    }

    public String getStringValues() {
        return stringValues;
    }

    public void setStringValues(String stringValues) {
        this.stringValues = stringValues;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
