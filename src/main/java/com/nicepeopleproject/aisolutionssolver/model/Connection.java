package com.nicepeopleproject.aisolutionssolver.model;

import jakarta.persistence.*;

@Entity
@Table(name = "node_connections")
public class Connection {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "from_node")
    private Integer fromNode;
    @Column(name = "to_node")
    private Integer toNode;
    @Enumerated(EnumType.STRING)
    @Column(name = "connection_type")
    private NodeConnectionTypeEnum connectionType;
    @Column(name = "description")
    private String description;


    public Connection() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFromNode() {
        return fromNode;
    }

    public void setFromNode(Integer fromNode) {
        this.fromNode = fromNode;
    }

    public Integer getToNode() {
        return toNode;
    }

    public void setToNode(Integer toNode) {
        this.toNode = toNode;
    }

    public NodeConnectionTypeEnum getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(NodeConnectionTypeEnum connectionType) {
        this.connectionType = connectionType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
