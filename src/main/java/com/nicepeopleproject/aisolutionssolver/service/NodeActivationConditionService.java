package com.nicepeopleproject.aisolutionssolver.service;

import com.nicepeopleproject.aisolutionssolver.model.NodeActivationCondition;

import java.util.List;

public interface NodeActivationConditionService {
    List<NodeActivationCondition> getAll();
}
