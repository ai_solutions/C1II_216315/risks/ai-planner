package com.nicepeopleproject.aisolutionssolver.service;

import com.nicepeopleproject.aisolutionssolver.model.KnowledgeBase;

import java.util.List;

public interface KnowledgeBaseService {
    List<KnowledgeBase> getAll();
}
