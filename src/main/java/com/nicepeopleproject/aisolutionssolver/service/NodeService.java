package com.nicepeopleproject.aisolutionssolver.service;

import com.nicepeopleproject.aisolutionssolver.model.Node;

import java.util.List;

public interface NodeService {
    List<Node> getAll();
}
