package com.nicepeopleproject.aisolutionssolver.service;

import com.nicepeopleproject.aisolutionssolver.model.Node;
import com.nicepeopleproject.aisolutionssolver.repository.NodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NodeServiceImpl implements NodeService{

    private NodeRepository nodeRepository;

    public NodeServiceImpl(NodeRepository nodeRepository) {
        this.nodeRepository = nodeRepository;
    }

    @Override
    public List<Node> getAll() {
        return nodeRepository.findAll();
    }
}
