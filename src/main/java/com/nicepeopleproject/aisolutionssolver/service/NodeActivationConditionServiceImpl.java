package com.nicepeopleproject.aisolutionssolver.service;

import com.nicepeopleproject.aisolutionssolver.model.NodeActivationCondition;
import com.nicepeopleproject.aisolutionssolver.repository.NodeActivationConditionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class NodeActivationConditionServiceImpl implements NodeActivationConditionService{
    private NodeActivationConditionRepository nodeActivationConditionRepository;

    public NodeActivationConditionServiceImpl(NodeActivationConditionRepository nodeActivationConditionRepository) {
        this.nodeActivationConditionRepository = nodeActivationConditionRepository;
    }

    @Override
    public List<NodeActivationCondition> getAll() {
        return nodeActivationConditionRepository.findAll();
    }
}
