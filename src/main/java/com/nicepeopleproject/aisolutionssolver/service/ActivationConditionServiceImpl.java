package com.nicepeopleproject.aisolutionssolver.service;

import com.nicepeopleproject.aisolutionssolver.model.ActivationCondition;
import com.nicepeopleproject.aisolutionssolver.repository.ActivationConditionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivationConditionServiceImpl implements ActivationConditionService{
    private ActivationConditionRepository activationConditionRepository;

    public ActivationConditionServiceImpl(ActivationConditionRepository activationConditionRepository) {
        this.activationConditionRepository = activationConditionRepository;
    }

    @Override
    public List<ActivationCondition> getAll() {
        return activationConditionRepository.findAll();
    }
}
