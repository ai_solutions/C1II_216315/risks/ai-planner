package com.nicepeopleproject.aisolutionssolver.service;

import com.nicepeopleproject.aisolutionssolver.model.Section;

import java.util.List;

public interface SectionService {
    List<Section> getAll();
}
