package com.nicepeopleproject.aisolutionssolver.service;

import com.nicepeopleproject.aisolutionssolver.model.ActivationCondition;

import java.util.List;

public interface ActivationConditionService {
    List<ActivationCondition> getAll();
}
