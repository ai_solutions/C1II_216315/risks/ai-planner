package com.nicepeopleproject.aisolutionssolver.service;

import com.nicepeopleproject.aisolutionssolver.model.Connection;

import java.util.List;

public interface ConnectionService {
    List<Connection> getAll();
}
