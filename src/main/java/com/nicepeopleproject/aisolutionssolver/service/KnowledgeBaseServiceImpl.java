package com.nicepeopleproject.aisolutionssolver.service;

import com.nicepeopleproject.aisolutionssolver.model.KnowledgeBase;
import com.nicepeopleproject.aisolutionssolver.repository.KnowledgeBaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class KnowledgeBaseServiceImpl implements KnowledgeBaseService {

    private KnowledgeBaseRepository  knowledgeBaseRepository;

    public KnowledgeBaseServiceImpl(KnowledgeBaseRepository knowledgeBaseRepository) {
        this.knowledgeBaseRepository = knowledgeBaseRepository;
    }

    @Override
    public List<KnowledgeBase> getAll() {
        return knowledgeBaseRepository.findAll();
    }
}
